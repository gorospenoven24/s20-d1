// JSON data format used by applications store and transport data to one another

/*JSON Objects
	syntax:
		{
			"propertyA": "valueA",
			"propertyB": "valueB",
			
		}
*/

/*{
	"city": "Quezon City",
	"province": "NCR",
	"country": "Philippines"
}*/

// Json arrays
/*"cities": [
       {"city": "Quezon", "province": "NCR", "country":"Philippines"},
       {"city": "caoocan City", "province": "NCR", "country":"Philippines"},
       {"city": "Marikina City", "province": "NCR", "country":"Philippines"}
      	
	]
*/
	// json Methods

	let batchesArray = [{bacthName: 'Batch X' },{bacthName: 'Batch y' }];

	// stringify method is used to covert js objects into string

	console.log('Result from stringify method');
	console.log(JSON.stringify(batchesArray));


// another structure of stringify or string format-------------
	let data = JSON.stringify({
		name: 'John',
		age: 31,
		address:{
			city: 'Manila',
			country:'Philippines'
		}
	});
	console.log(data);
	// console.log(JSON.stringify(data));



// Using stringify method with variables 


// use details
/*let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age');

let address ={
	city: prompt('which city do you live in'),
	country: prompt('which country does your city address belong to')
};
let otherData = JSON.stringify({
	firstName: firstName,
	lastName:lastName,
	age:age,
	address: address
});
console.log(otherData);
*/

// converting stringify JSON into javascript objects------
// convert into typical obejct using stringify date

let batchesJSON = `[{"batchName": "Batch X" },{"batchName": "Batch Y" }]`;
console.log("Result form parse method");
// to parse JSOn in typical Obejct
console.log(JSON.parse(batchesJSON)); 

let stringifiedObject = JSON.parse(`{"name": "John", "age": "30", "address": {"city":"manila", "country": "Philippines"}}`);
console.log(stringifiedObject);